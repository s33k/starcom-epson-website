from tkinter import *
from tkinter import messagebox
from tkinter.ttk import Progressbar
from lxml import etree
import requests
from lxml.html.clean import unicode

header = '''
<style>
.spacer{
    border-bottom: 1px solid #eee;
    margin: 20px 0;
}
</style>

<div class='container-fluid'>
    <div class="row">
        <a class="col-xs-12" href="#tech"><h4 class="text-center"><strong>Dane Techniczne</strong></h4></a>
    </div>
    <div class="row spacer"></div>
    <div class='page-header'>
        <h1 class="text-center" style="font-size: 3em;"><strong> {{name}} </strong></h1>
    </div>
    <div class="row">
        <div>
            <p class="text-center lead">
                {{short_desc}}
            </p>
        </div>
    </div>
'''
black_white_print = '''
<div class="row">
        <div class="col-xs-12 col-md-6">
            <h1 class="text-center" style="font-size: 3.5em;"><strong> {{black_pages}} </strong></h1>
            <h2 class="text-center">Czarno-białych stron na miesiąc</h2>
        </div>
        <div class="col-xs-12 col-md-6">
            <h1 class="text-center" style="font-size: 3.5em;"><strong> {{black_speed}} Str./min</strong></h1>
            <h2 class="text-center">Prędkość drukowania</h2>
        </div>
    </div>
'''
color_print = '''
<div class="row">
        <div class="col-xs-12 col-md-6">
            <h1 class="text-center text-primary" style="font-size: 3.5em;"><strong> {{color_pages}} </strong></h1>
            <h2 class="text-center text-primary">Kolorowych stron na miesiąc</h2>
        </div>
        <div class="col-xs-12 col-md-6">
            <h1 class="text-center text-primary" style="font-size: 3.5em;"><strong> {{color_speed}} Str./min</strong></h1>
            <h2 class="text-center text-primary">Prędkość drukowania</h2>
        </div>
    </div>
'''
left_desc = '''
<br>
    <div class="row">
        <div class="col-xs-12 col-md-6 text-primary text-center"><p style="font-size: 2.5em; padding: 1em 0;"> {{desc_name}} </p></div>
        <div class="col-xs-12 col-md-6"><p style="font-size: 1.25em;"> {{desc_body}} </p></div>
    </div>
'''
right_desc = '''
<br>
    <div class="row">
        <div class="col-xs-12 col-md-6"><p style="font-size: 1.25em;"> {{desc_body}} </p></div>
        <div class="col-xs-12 col-md-6 text-primary text-center"><p style="font-size: 2.5em; padding: 1em 0;"> {{desc_name}} </p></div>
    </div>
'''
key_header = '''
<div class="row">
        <h1 class="text-center" style="font-size: 2.5em;">Kluczowe cechy</h1>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <table class="table">
                    <tbody>
'''
key_template = '''
<tr>
    <td><h1 class="text-primary"> {{key_name}} </h1></td>
    <td><p style="font-size: 1.15em;"> {{key_desc}} </p></td>
</tr>
'''
key_footer = '''
</tbody>
                </table>
            </div>
        </div>
    </div>
'''
tech_date_header = '''
<div id="tech" class="row">
        <h1 class="text-center" style="font-size: 2.5em;">Dane Techniczne</h1>
    </div>
'''
tech_data_row_header = '''
<div class="row">
        <div class="col-xs-12">
            <h2 style="font-size: 1.75em;"> {{name}} </h2>
            <div class="table-responsive">
                <table class="table">
                    <tbody>
'''
tech_data_row = '''
<tr>
                            <td><strong> {{data_name}} </strong></td>
                            <td>{{data_body}}</td>
                        </tr>
'''
tech_data_footer = '''
</tbody>
                </table>
            </div>
        </div>
    </div>
'''
footer = '''
<div>
        <p style="font-size: .75em; color:rgb(161, 161, 161)">1 W porównaniu do kartridża o najniższej wydajności używanego w urządzeniu z listy 20 najpopularniejszych drukarek atramentowych A4 na podstawie raportu IDC o liczbie wydruków w regionie EMEA (4 kw. 2016 r.) w okresie od stycznia do grudnia 2016 roku. Wynik uzyskano na podstawie standardowej wydajności wkładu konkurencyjnej firmy podzielonej przez średnią wydajność atramentu dołączonego do drukarki EcoTank ITS firmy Epson. 2. Atrament na trzy lata przy średniej liczbie 290 wydrukowanych stron na domowych i biurowych drukarkach EcoTank ITS. Obliczenia na podstawie najniższej wydajności druku monochromatycznego na fabrycznych zasobnikach dla następujących drukarek L4150, L4160, L6160, L6170, L6190. 3. Podana wydajność została ustalona za pomocą oryginalnej metodologii firmy Epson na podstawie symulacji druku wzorców testowych zgodnie z normą ISO/IEC 24712. Podana wydajność NIE jest oparta na normie ISO/IEC 24711. Podana wydajność może różnić się w zależności od drukowanych obrazów, rodzaju używanego papieru, częstotliwości drukowania oraz warunków pracy, np. temperatury. Podczas wstępnej konfiguracji drukarki pewna ilość atramentu zostaje zużyta do zapełnienia dysz głowicy drukującej. Z tego powodu wydajność początkowego zestawu może być niższa. 4. Ustalono zgodnie z normą ISO/IEC 24734 obrazującą średnią ESAT testów kategorii biznesowej dla druku jednostronnego. Więcej informacji na stronie www.epson.eu/testing. 5. Obowiązują określone warunki i postanowienia. Więcej informacji można znaleźć na stronie www.epson.pl/gwarancjanadrukarki 6.Wymaga bezprzewodowego połączenia z Internetem. Więcej informacji o produkcie, obsługiwanych językach i urządzeniach można znaleźć na stronie www.epson.pl/connect.</p>    </div>
</div>
'''

def generate():
    progress['value'] = 0
    
    if link_field.get() is None:
        messagebox.showerror("Error", "Nie podałeś linku do drukarki")
        return

    progress['value'] = 10
    try:
        page = requests.get(link_field.get())
        progress['value'] = 20
    except:
        return messagebox.showerror("Error", "Problem z ładowaniem strony")

    parser = etree.HTMLParser()
    content = unicode(page.content, 'utf-8')
    content = content.replace("<sup>1</sup>", "")
    content = content.replace("<sup>2</sup>", "")
    content = content.replace("<sup>3</sup>", "")
    content = content.replace("<sup>4</sup>", "")
    content = content.replace("<sup>5</sup>", "")
    content = content.replace("<sup>", "")
    content = content.replace("</sup>", "")

    page_html = etree.fromstring(content, parser=parser)
    progress['value'] = 25

    name = page_html.xpath('//h1[@itemprop="name"]/text()')[0]
    printer_name = name

    short_description = page_html.xpath('//p[@class="headline"]/text()')
    short_description = short_description[0]
    progress['value'] = 30

    descriptions = page_html.xpath('//div[@class="columns s-sixteen l-eight six"]')
    descriptions += page_html.xpath('//div[@class="columns s-sixteen eight offset-by-two"]')
    descriptions_map = {}
    for desc in descriptions:
        children = desc.getchildren()
        if len(children) == 2:
            desc_name = children[0].getchildren()[0].text
            desc_detail = children[1].text
            descriptions_map[desc_name] = desc_detail
    progress['value'] = 35

    key_feature = page_html.xpath('//div[@class="key-features"]')[0].getchildren()[0].getchildren()[1].getchildren()
    keys = {}
    for key in key_feature:
        text = key.text.split(':')
        keys[text[0]] = text[1]
    progress['value'] = 40

    tech_data_divs = page_html.xpath('//ul[@class="collapsible-list"]')[0]
    tech_data = {}
    for data in tech_data_divs:
        data_name = data.getchildren()[0].text
        data_map = {}
        div = data.getchildren()[1].getchildren()[0].getchildren()
        for row in div:
            row_children = row.getchildren()
            row_head = row_children[0].getchildren()[0].text
            row_body = row_children[1].text
            data_map[row_head] = row_body
        tech_data[data_name] = data_map
    progress['value'] = 45

    output = ''
    output += header
    output = output.replace("{{name}}", name)
    output = output.replace("{{short_desc}}", short_description)
    output += "<br><br>"

    progress["value"] = 50
    help_index = 0
    for name in descriptions_map.keys():
        help_index += 1
        if help_index % 2 == 1:
            output += left_desc
        else:
            output += right_desc
        output = output.replace("{{desc_name}}", name)
        if descriptions_map[name] is None:
            descriptions_map[name] = ''
        output = output.replace("{{desc_body}}", descriptions_map[name])

    output += "<br><br>"

    if len(keys) > 0:
        output += key_header
        for name in keys.keys():
            output += key_template
            output = output.replace("{{key_name}}", name)
            output = output.replace("{{key_desc}}", keys[name])
        output += key_footer

    progress["value"] = 70

    if tech_data is not None:
        output += tech_date_header
        for name in tech_data.keys():
            output += tech_data_row_header
            output = output.replace("{{name}}", name)
            for key in tech_data[name].keys():
                output += tech_data_row
                output = output.replace("{{data_name}}", key)
                output = output.replace("{{data_body}}", tech_data[name][key])
            output += tech_data_footer

    output += footer
    output = output.replace("\xb2", "")
    output = output.replace("\u200e", "")
    progress["value"] = 90

    file = open("%s.html" % printer_name, "w")
    file.write(output)
    file.close()

    progress["value"] = 100


window = Tk()
window.geometry('360x110')
window.title("Jebać drukarki (╯°□°）╯︵ ┻━┻  wersja EPSON")

title_label = Label(window, text="Epson", font="Arial 10 bold")
title_label.grid(column=0, row=0, sticky=W)

link_label = Label(window, text="Link do strony drukarki")
link_label.grid(column=0, row=1, columnspan=2, sticky=W)

link_field = Entry(window, width=30)
link_field.grid(column=2, row=1, columnspan=2, sticky=W)

btn = Button(window, text="Generuj stronę", command=generate, width=35)
btn.grid(column=0, row=6, columnspan=5, pady=5)

progress = Progressbar(window, length=360)
progress.grid(column=0, row=7, columnspan=4, sticky=W)

window.mainloop()
